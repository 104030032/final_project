var loadState = { 
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width/2, 90+150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar 
        var progressBar = game.add.sprite(game.width/2, 90+200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);
        game.load.audio('audio1', 'img/bgm1.ogg');

        game.load.tilemap('map', 'assets/map5.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.image('tile1', 'assets/wall.png');
        game.load.image('tile2', 'assets/chest.png');

        game.load.spritesheet('player', 'assets/player2.png', 48, 48);
        game.load.spritesheet('chest', 'assets/chest.png', 64, 64);//tile大小同
        game.load.spritesheet('explosion','assets/explosion.png',96,96);
        game.load.image('nail1','assets/nail1.png');
        game.load.image('nail2','assets/nail2.png');
        game.load.image('hole','assets/hole.png');

        game.load.image('box', 'assets/box.png');
        game.load.image('machine', 'assets/machine.png');
        game.load.image('battery', 'assets/battery.png');
        game.load.image('tool1', 'assets/tool1.png');
        game.load.spritesheet('boom', 'assets/boom.png',65,65);
        game.load.spritesheet('electric', 'assets/electric.png',25,514);
    }, 
    create: function() { 
        this.ref = firebase.database().ref(game.global.room_name);
        this.ref.once("value").then(function(snapshot){
            var total_ready = 0;
            var j = 0;
            snapshot.forEach(function(childSnapshot) {
                var ready = childSnapshot.child("public").val().ready
                var my_number = childSnapshot.child("public").val().my_number;
                total_ready += ready;
                game.global.player[my_number] = childSnapshot.key;
                if( childSnapshot.key == user_name ) game.global.my_number = my_number;
                j++;
            });
            for(;j<4;j++){
                game.global.player[j] = "";
            }
            if( total_ready == snapshot.numChildren() ) {
                if( game.global.my_number != 5 ) game.state.start('play');
                else {
                    alert("this game is playing.")
                    changeto_gamemode();
                }
            }
            else if( snapshot.numChildren()<4 || game.global.my_number != 5 ) game.state.start('waiting');
            else {
                alert("The number of people has reached the limit.")
                changeto_gamemode();
            }
        });
    }
};