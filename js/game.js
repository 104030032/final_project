var game = null;
function changeto_gamemode(){
    if(game) { game.destroy(); game = null; }
    document.getElementById('chat').innerHTML = 
    '<div class="container text-center">'+
        '<div id="canvas">'+
            '<p class="setting-form">'+
                '<a>Which room do you want to enter?</a>'+
                '<input type="text" id="room_name" class="form-control" placeholder="" autofocus>'+
                '<button class="btn btn-lg btn-primary btn-block" onclick="gammode()">sent</button>'+
            '</p>'+
        '</div>'+
   '</div>';
}
function gammode(){
    var txtname = document.getElementById('room_name');
    if(!txtname.value) alert("Please enter room name");
    else{
        document.getElementById('chat').innerHTML = "<div id='canvas'></div>"
        // Initialize Phaser 
        game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
        // Define our global variable
        game.global = { room_name : "" , player : ["","","",""] , my_number : 5 }; 
        game.global.room_name = "game_rooms/";
        game.global.room_name += txtname.value;
        game.global.my_number = 5;
        // Add all the states 
        game.state.add('boot', bootState); 
        game.state.add('load', loadState); 
        game.state.add('waiting', waitingState); 
        game.state.add('play', playState); 
        // Start the 'boot' state 
        game.state.start('boot');
    }
}