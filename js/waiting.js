var waitingState = { 
    create: function(){
        this.name_array = [ 0 , 0 , 0 , 0 ]//name不能為0
        this.ready = false;
        this.ref = firebase.database().ref(game.global.room_name);
        this.set_current_data();
    },
    set_current_data: function(){
        this.ref.child(user_name+"/public").set({
            'ready': waitingState.ready
        }).then(function(e){
            waitingState.ref.once("value").then(waitingState.update_current_data);
            var txt = game.add.text( game.width/2 , game.height/2-50-25, "Waiting for other player..." , 
                                     { font: '25px Arial', fill: '#ffffff' });
            txt.anchor.setTo(0.5, 1); 
            waitingState.txt = game.add.text( game.width/2 , game.height/2+75+25, "Press 'r' to get ready" , 
                                              { font: '25px Arial', fill: '#ffffff' });
            waitingState.txt.anchor.setTo(0.5, 1); 
            var keyr = game.input.keyboard.addKey(Phaser.Keyboard.R);
            keyr.onDown.add(function(){waitingState.change_get_ready();}, waitingState); 
        });
    },
    update_current_data: function(snapshot){
        var n = snapshot.numChildren();
        var number_array = [];
        var i = 0, j;
        while(i<n){
            var random = Math.floor(n*Math.random());
            if(number_array.every(function(currentValue){return (currentValue != random);})){
                number_array[i] = random;
                i++;
            }
        }
        i = 0;
        snapshot.forEach(function(childSnapshot) {
            waitingState.ref.child(childSnapshot.key+"/public").update({'my_number': number_array[i]});
            i++;
        });
        waitingState.ref.on("value",waitingState.on_ref_value_change);
    },
    on_ref_value_change: function(snapshot){
        var i = 0,j,total_ready = 0;
        snapshot.forEach(function(childSnapshot) {
            var ready = childSnapshot.child("public").val().ready;
            var my_number = childSnapshot.child("public").val().my_number;
            var txt = childSnapshot.key;
            txt += (ready) ? " ready" : " not ready";
            total_ready += ready;

            game.global.player[my_number] = childSnapshot.key;
            if( waitingState.name_array[i] !== 0 ) waitingState.name_array[i].destroy();
            waitingState.name_array[i] =
                game.add.text( game.width/2 , game.height/2-50+25*(i+1) , txt , { font: '25px Arial', fill: '#ffffff' });
            waitingState.name_array[i].anchor.setTo(0.5, 1); 
            if( childSnapshot.key == user_name ) game.global.my_number = my_number;
            i++;
        });
        for(j=i;j<4;j++){
            game.global.player[j] = "";
        }
        if( total_ready == i ) {
            waitingState.ref.off("value",waitingState.on_ref_value_change);
            game.state.start('play');
        }
    },
    change_get_ready: function(){
        this.ready = !this.ready;
        var new_txt = (this.ready) ? "Press 'r' to cancel" : "Press 'r' to get ready" ;
        this.txt.setText(new_txt);
        this.ref.child(user_name+"/public").update({'ready': waitingState.ready});
    }
};
 