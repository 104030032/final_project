var little = 10;
var txt_height = 20;
var limit = 10;
var linecolor =  0xff8800;
//6/12更新(其實是6/13寫的哈哈)
//更新update、whichroom、getChestPosition、nailtrap、holetrap、explode等(忘記了)

//6/13更新
//主要就是合入昨天給我的code
//(更動地方有:create、update、create_map、create_chest、create_message、create_timer、nailtrap、nail_end、holetrap、
//           explode、render、endTimer、formatTime)
//chest_event 由於我覺得沒有設全域變數的必要所以弄進函數內了
//另外有修正可以有負數電池的bug 不過其他item_effect尚未更新
//movePlayer 目前可以斜走 並可以在斜走時播動畫
//在load.js的部分 有更新房間人數可以超過4人的問題
//在game.js的部分 加入game的宣告 和changeto_gamemode()的變更

//array that will store each chest
//for timer
var timer, timerEvent, text;

var playState = {
    create: function(){
        game.stage.backgroundColor = '#000000'; 
        //chat
        this.userref = firebase.database().ref("game_rooms/"+game.global.room_name+"/"+user_name);
        this.talking = false;
        this.talk_to = "";
        this.talk_to_ref = "";
        this.talk_box_txt = "";
        this.talk_box = 0;
        this.message_array = [ 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0];

        this.currentOn = false;

        this.nailtrigger = 0;
        this.CHEST = new Array(81);
        //determine trap event
        this.movable = 1;

        this.create_map();
        this.setchat();
        this.setkeys();
        this.setitems();
        this.setplayers();
        //20180609
        this.create_chest();
        this.create_message();
        this.create_timer();

        this.userref.child("message").orderByChild("createdAt").on("value",playState.get_message);
    },
    update: function() {
        //item_effect
        //蔡
        //20180609 chest collide + nail_1,nail_2 collide
        for(i=0;i<81;i++)
        {
            game.physics.arcade.collide(this.player, this.CHEST[i]);
        } 
        if(this.nailtrigger){
            game.physics.arcade.collide(this.player, this.nail_1,this.death, null, this);
            game.physics.arcade.collide(this.player, this.nail_2,this.death, null, this);
            game.physics.arcade.collide(this.nail_1, this.nail_2,this.nail_end,null,this); //nail_1,nail_2 collide
        }
        //徐
        game.physics.arcade.collide(this.player,this.boom,this.boomEffect, null, this);
        game.physics.arcade.collide(this.player,this.box,this.boxEffect, null, this);
        game.physics.arcade.collide(this.player,this.machine1,this.machineEffect, null, this);
        game.physics.arcade.collide(this.player,this.machine2,this.machineEffect, null, this);
        game.physics.arcade.collide(this.player,this.battery1,this.batteryEffect, null, this);
        game.physics.arcade.collide(this.player,this.battery2,this.batteryEffect, null, this);
        game.physics.arcade.collide(this.player,this.tool1,this.tool1Effect, null, this);
        if(this.currentOn){
            game.physics.arcade.collide(this.player,this.electric,this.shockEffect);
        }
        //map
        this.movePlayer();
        game.physics.arcade.collide(this.player, this.layer);
        //徐
        this.SetBoom(this.player);
        //蔡
        this.gameOver();
    },

    setkeys: function(){
        //chat
        var keyt = game.input.keyboard.addKey(Phaser.Keyboard.T);//talk
        keyt.onDown.add(this.change_mode, this); 
        var keyy = game.input.keyboard.addKey(Phaser.Keyboard.Y);//sent
        keyy.onDown.add(this.sent_message, this); 
        //item_effect
        //蔡
        var Opens = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); 
        Opens.onDown.add(this.checkBox, this);
        var trigger = game.input.keyboard.addKey(Phaser.Keyboard.E);
        trigger.onDown.add(this.explode, this);
        var needle =  game.input.keyboard.addKey(Phaser.Keyboard.N);
        needle.onDown.add(this.nailtrap,this);
        var blackhole =  game.input.keyboard.addKey(Phaser.Keyboard.H);
        blackhole.onDown.add(this.holetrap,this);  
        //徐
        this.keyboard = game.input.keyboard.addKeys({
            'b': Phaser.Keyboard.B,
            'c': Phaser.Keyboard.C
        });
        //chat
        this.key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.key1.onDown.add(function() {this.using_number_key(1);}, this); 
        this.key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.key2.onDown.add(function() {this.using_number_key(2);}, this); 
        this.key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        this.key3.onDown.add(function() {this.using_number_key(3);}, this); 
        this.key4 = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        this.key4.onDown.add(function() {this.using_number_key(4);}, this); 
        this.key5 = game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
        this.key5.onDown.add(function() {this.using_number_key(5);}, this); 
        this.key6 = game.input.keyboard.addKey(Phaser.Keyboard.SIX);
        this.key6.onDown.add(function() {this.using_number_key(6);}, this); 
        this.key7 = game.input.keyboard.addKey(Phaser.Keyboard.SEVEN);
        this.key7.onDown.add(function() {this.using_number_key(7);}, this); 
        this.key8 = game.input.keyboard.addKey(Phaser.Keyboard.EIGHT);
        this.key8.onDown.add(function() {this.using_number_key(8);}, this); 
        
        //key inputs
        this.cursor =game.input.keyboard.createCursorKeys();
    },
    setplayers: function(){
        //player create
        this.player = game.add.sprite(200, 200, 'player');
        this.player.direction = 1;
        this.player.boom=0;
        this.player.battery=0;
        this.player.tool1=0;
        game.physics.arcade.enable(this.player);
        this.player.body.setSize(32, 32, 8, 8);
        this.player.body.collideWorldBounds = true;
        // 鏡頭跟隨玩家
        this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);

        this.player.animations.add('downwalk',  [ 7, 8, 7, 9], 8, true);
        this.player.animations.add('leftwalk',  [ 0, 1, 2, 3], 8, true);
        this.player.animations.add('rightwalk', [12,13,14,15], 8, true);
        this.player.animations.add('upwalk',    [19,20,19,21], 8, true);
        this.player.animations.add('dead', [29],  8, false);
    },
    movePlayer: function() {
        this.player.body.velocity.x = 0
        this.player.body.velocity.y = 0
        if(this.movable) {
            if (this.cursor.left.isDown) {
                this.player.body.velocity.x = -250;
                this.player.direction = 2;
            }
            if(this.cursor.right.isDown) {
                this.player.body.velocity.x = 250;
                this.player.direction = 3;
            }    
            if(this.cursor.up.isDown){
                this.player.body.velocity.y = -250;
                this.player.direction = 0;
            }  
            if(this.cursor.down.isDown){
                this.player.body.velocity.y = 250;
                this.player.direction = 1;
            }    
            switch(this.player.direction) {
                case 0: this.player.animations.play('upwalk'); break;
                case 1: this.player.animations.play('downwalk'); break;
                case 2: this.player.animations.play('leftwalk'); break;
                case 3: this.player.animations.play('rightwalk'); break;
            }
        }       
        // If player is not walking
        if(!this.player.body.velocity.x && !this.player.body.velocity.y) {
            switch(this.player.direction) {
                case 0:
                    this.player.frame=19;
                    break;
                case 1:
                    this.player.frame=7;
                    break;
                case 2:
                    this.player.frame=0;
                    break;
                case 3:
                    this.player.frame=12;
                    break;
            }
            this.player.animations.stop();
        }   
    },
    //map
    create_map: function(){
        // 新增地圖
        this.map = this.game.add.tilemap('map')
    
        // 新增圖塊 addTilesetImage( '圖塊名稱' , 'load 時png的命名' )
        this.map.addTilesetImage('wall', 'tile1')
        this.map.addTilesetImage('chest', 'tile2')
    
        // 建立圖層 (圖層名稱為 tile 中所設定)
        this.layer = this.map.createLayer('layer')
        this.layer.resizeWorld()
        //collide
        this.map.setCollision(592);
        this.map.setCollision(668);
        this.map.setCollision(670);
        this.map.setCollision(632);
        this.map.setCollision(665);
        this.map.setCollision(672);
        this.map.setCollision(674);
        this.map.setCollision(707);
        this.map.setCollision(711);
        this.map.setCollision(713);
        this.map.setCollision(717);
        this.map.setCollision(721);
        this.map.setCollision(1186);
        
        this.map.setTileIndexCallback(680, this.disappear, this);
        this.map.setTileIndexCallback(593, this.appear, this);
    },
    create_chest: function(){
        for(i=0;i<81;i++)
        {   
            if(i==0||i==8||i==72||i==80)continue; //四個角落不放寶箱
            var chest_pos=this.getChestPosition(i)
            this.CHEST[i]=game.add.sprite(chest_pos.x, chest_pos.y, 'chest');
            this.CHEST[i].anchor.setTo(0.5, 0.5);
            this.CHEST[i].height=64; 
            game.physics.arcade.enable(this.CHEST[i]);
            this.CHEST[i].body.immovable = true;
            this.CHEST[i].animations.add('open',[54,66,78,90],2,false)
            this.CHEST[i].frame=54;
        } 
    },
    create_message: function(){
        text = game.add.text(3*game.width/4, game.height-40, 
            "", { font: '25px Arial', fill: '#ff0000' });
        text.anchor.setTo(0.5,0.5);
        text.fixedToCamera = true;
        var g = game.add.graphics(0, 0);        
        g.lineStyle(2, linecolor, 1);
        g.drawRoundedRect(game.width/2, game.height-6*little-txt_height,game.width/2-10, 5*little+txt_height, 10);
        g.fixedToCamera = true;
    },
    create_timer: function(){
        timer = game.time.create();
        timerEvent = timer.add(Phaser.Timer.MINUTE * 15 + Phaser.Timer.SECOND * 0, this.endTimer, this);
        timer.start();   
    },  
    //房間右跟下的門屬於這房間，左跟上的門屬於其他房間
    //range:0~80
    whichroom:function(){
        return Math.floor((this.player.y-207)/640)*9+Math.floor((this.player.x-79)/640) ;   
    },
    //輸入房間號碼,回傳箱子座標
    getChestPosition:function(num){
        var pos={
            x:288+(num%9)*640,
            y:416+(Math.floor(num/9))*640
        }
        return pos;
    },
    //chat
    setchat: function(){
        //聊天視窗
        this.graphics = game.add.graphics(0, 0);
        this.graphics.lineStyle(2, linecolor, 1);
        this.graphics.drawRoundedRect(little, game.height-3*little-txt_height,
                                      game.width/2-2*little, 2*little+txt_height, 10);
        this.graphics.fixedToCamera = true;

        //訊息視窗
        var g = game.add.graphics(0, 0);
        g.lineStyle(2, linecolor, 1);
        g.drawRect(2*little, game.height-2*little-txt_height, game.width/2-4*little,txt_height);
        g.fixedToCamera = true;
    },
    change_mode: function(){
        var i;
        this.talking = !this.talking;
        this.graphics.destroy();
        this.graphics = game.add.graphics(0, 0);
        this.graphics.lineStyle(2, linecolor, 1);
        this.graphics.fixedToCamera = true;
        if(this.talking){
            this.graphics.drawRoundedRect(little, game.height/2+little,
                                          game.width/2-2*little, game.height/2-2*little, 10);
            for(i=0;i<limit;i++) if(this.message_array[i]) this.message_array[i].visible = true;
        }
        else {
            this.graphics.drawRoundedRect(little, game.height-3*little-txt_height,
                                          game.width/2-2*little, 2*little+txt_height, 10);
            for(i=0;i<limit;i++) if(this.message_array[i]) this.message_array[i].visible = false;
        }
    },
    using_number_key: function(number_key){
        if(this.talking){
            if( number_key < 5 ){
                if( number_key - 1 != game.global.my_number ){
                    if(game.global.player[number_key-1]){
                        this.talk_to = game.global.player[number_key-1];
                        this.talk_box_txt = "To " + this.talk_to + " : ";
                    }
                }
            }
            else{
                var message;
                switch(number_key){
                    case 5: message = "wow"; break;
                    case 6: message = "ya"; break;
                    case 7: message = "hello"; break;
                    case 8: message = "nooooooo"; break;
                }
                if(this.talk_box_txt) this.talk_box_txt = this.talk_box_txt.split(':')[0] + ": " + message;
            }
            this.reset_talk_box();
        }
    },
    get_message: function(snapshot) {
        var num = snapshot.numChildren();
        var i = 0;
        snapshot.forEach(function(childSnapshot) {
            if( num-i > limit ) 
            firebase.database().ref("game_rooms/"+game.global.room_name+"/"+user_name+
                                    "/message/"+childSnapshot.key).remove();
            else{
                var childData = childSnapshot.val();
                var txt = ( ( childData.player==user_name ) ? "" : childData.player + " : ") + childData.text;
                if(playState.message_array[i]) playState.message_array[i].destroy();
                playState.message_array[i] = game.add.text( 2*little , game.height-2*little-txt_height*(num-i+2)+3,
                                                            txt , { font: '18px Arial', fill: '#ffffff' });
                playState.message_array[i].fixedToCamera = true;
                if(!playState.talking){
                    playState.message_array[i].visible = false;
                }
            }
            i++;
        });
    },
    sent_message: function(){
        if(this.talk_box_txt){
            currenttime = firebase.database.ServerValue.TIMESTAMP;
            this.talk_to_ref = firebase.database().ref("game_rooms/" + game.global.room_name + "/" + 
                                                        this.talk_to + "/message");
            var talk_box_txt = this.talk_box_txt;
            this.talk_to_ref.push().set({
                'player': user_name,
                'text': talk_box_txt.split(':')[1],
                'createdAt': currenttime
            })
            this.userref.child("message").push().set({
                'player': user_name,
                'text': talk_box_txt,
                'createdAt': currenttime
            })
            this.talk_box_txt = this.talk_box_txt.split(':')[0] + ": ";
            this.reset_talk_box();
        }
    },
    reset_talk_box: function(){
        if(this.talk_box) this.talk_box.destroy();
        this.talk_box = game.add.text(2*little, game.height-2*little-txt_height+3,
                                      this.talk_box_txt, { font: '18px Arial', fill: '#ffffff' });     
        this.talk_box.fixedToCamera = true;
    },
    //以下皆為item_effect
    setitems: function(){   
        //徐
        //boom create
        this.boom = game.add.sprite( 128, 192, 'boom');
        this.boom.safe=true;
        game.physics.arcade.enable(this.boom);
        this.boom.body.immovable = true;
        this.boom.frame=0;
        this.boom.body.setSize(22, 30, 5, 35);

        //box create
        this.box=game.add.sprite(192,192, 'box');
        game.physics.arcade.enable(this.box);

        //machine create
        this.machine1 = game.add.sprite(game.width/2,192, 'machine');
        this.machine2 = game.add.sprite(game.width/2,game.height-48, 'machine');
        this.machine1.power=false;
        this.machine2.power=false;
        game.physics.arcade.enable(this.machine1);
        game.physics.arcade.enable(this.machine2);
        this.machine1.body.immovable = true;
        this.machine2.body.immovable = true;

        //battery create
        this.battery1= game.add.sprite(256,200, 'battery');
        game.physics.arcade.enable(this.battery1);
        this.battery1.body.immovable = true;
        this.battery2= game.add.sprite(256,400, 'battery');
        game.physics.arcade.enable(this.battery2);
        this.battery2.body.immovable = true;

        //tool create
        this.tool1=game.add.sprite(160,300, 'tool1');
        game.physics.arcade.enable(this.tool1);
        this.tool1.body.immovable = true;

        this.electric = 0;
    },
    //蔡
    //開寶箱時是否要使玩家不能動
    checkBox: function(){
        //各房間開啟寶箱+event 觸發
        var room = this.whichroom();
        var pos= this.getChestPosition(room);
        if(this.player.y<(pos.y+60)&& this.player.y>pos.y&&this.player.x<(pos.x+32)&&this.player.x>pos.x-32)
        {
            if(this.CHEST[room].alive){
                this.CHEST[room].alive = false;
                var chest_event = Math.floor((Math.random() * 3) + 1);
                this.CHEST[room].animations.play('open');
                game.camera.flash(0xffffff, 3000);
                game.time.events.add(3000, function() {
                    this.CHEST[room].kill();
                    if(chest_event==1) this.explode();
                    else if (chest_event==2)this.nailtrap();
                    else this.holetrap();
                },this);
            }
        }
    },
    explode: function(){
        this.movable=0;
        var that = this.bomb = game.add.sprite(this.player.x, this.player.y, 'explosion');
        this.bomb.anchor.setTo(0.5,0.5);
        this.bomb.width=200;
        this.bomb.height=200;
        this.bomb.animations.add('BOOM',  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
        this.bomb.animations.play('BOOM');
        game.camera.shake(0.02, 2100);
        game.time.events.add(2000, function() {
            that.destroy();
            this.death();
        },this);
    },
    //多人時，他人的nail也可以刺我嗎? 若是，改成array存多組nail較好
    nailtrap: function(){
        //nail create 改到這
        var room = this.whichroom();
        var pos= this.getChestPosition(room);
        this.nail_1= game.add.sprite(pos.x,pos.y-64*3,'nail1');
        this.nail_1.width=448;
        this.nail_1.anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.nail_1);
        this.nail_1.body.immovable = true;
        this.nail_2= game.add.sprite(pos.x,pos.y+64*3,'nail2');
        this.nail_2.width=448;
        this.nail_2.anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.nail_2);
        this.nail_2.body.immovable = true;
        this.nailtrigger=1;
        this.nail_1.body.gravity.y=40;
        this.nail_2.body.gravity.y=-40;
        text.setText('nail trap activated');    //新增text
        //過一段時間,消除text
        game.time.events.add(4500, function() {
            text.setText('');
        },this);
    },
    nail_end:function(nail_1,nail_2){
        nail_1.destroy();
        nail_2.destroy();
    },
    holetrap: function(){
        var randoms= Math.floor((Math.random() * 80));  //random 房間
        this.hole = game.add.sprite(this.player.x, this.player.y, 'hole');
        this.hole.anchor.setTo(0.5,0.5);
        game.world.swap(this.hole, this.player);
        movable=0;
        text.setText('hole trap activated');    //新增text
        //過一段時間,消除text
        game.time.events.add(1100, function() { 
            text.setText('');
        },this);
        game.add.tween(this.hole).to({angle: -180}, 2000).loop().start();
        game.add.tween(this.player.scale).to({x: 0, y:0}, 1000).start();
        game.time.events.add(1100, function() {
            this.hole.destroy();
            movable=1;
            game.add.tween(this.player.scale).to({x: 1, y:1}, 1000).start()
            this.player.x=this.getChestPosition(randoms).x;    //random 房間
            this.player.y=this.getChestPosition(randoms).y+64; //random 房間
        },this);
    },
    death:  function(){
        this.player.alive = false;
    },
    gameOver: function(){
        if(!this.player.alive){
            this.player.animations.play('dead');
            game.time.events.add(200, function() {
                alert("You're DEAD!");
                location.reload();
            },this);
        }
    },

    disappear:function(){
        this.player.alpha = 0;
    },
    appear: function(){
        this.player.alpha = 1;
    },
    //徐
    SetBoom: function(player){
        if( this.keyboard.b.isDown && player.boom>0 ){
            var x = player.body.position.x;
            var y = player.body.position.y;
            if(player.direction==3) {
                x=x+40;
            }
            else if(player.direction==2) {
                x=x-40;
            }
            else if(player.direction==1) {
                y=y+40;
            }
            else{
                y=y-40;
            }

            //boom
            this.boom = game.add.sprite(x,y, 'boom');
            this.boom.safe=false;
            game.physics.arcade.enable(this.boom);
            this.boom.body.setSize(22, 30, 5, 35);
            this.boom.body.immovable = true;
            this.boom.animations.add('explosion',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],12,false,true);
            this.boom.frame=0;

            player.boom-=1;
            console.log(player.boom);  
        }  
    },
    //collide effect
    boxEffect: function(player, box){
        box.body.velocity.x = 0;
        box.body.velocity.y = 0;
        if(player.direction==3) {
            box.x+=5;
        }
        else if(player.direction==2) {
            box.x-=5;
        }
        else if(player.direction==1) {
            box.y+=5;
        }
        else{
            box.y-=5;
        }
    },
    boomEffect: function(player, boom){
        if(this.keyboard.c.isDown&&boom.safe==true){
            boom.kill();
            player.boom+=1;
            console.log(player.boom);  
        }
        else if(this.keyboard.c.isDown&&boom.safe==false){
            boom.animations.play('explosion'); 
            boom.body.setSize(0, 0, 0, 0);
            console.log(player.boom);  
        }
       
    },
    machineEffect:function(player, machine){
        if( this.keyboard.b.isDown && machine.power==false && player.battery > 0 ){
            player.battery-=1;
            machine.power=true;
            console.log(player.battery);  
            if(!this.electric){
                if( ( machine==this.machine1 && this.machine2.power==true ) ||
                    ( machine==this.machine2 && this.machine1.power==true ) ){
                    //生成电流
                    this.electric = game.add.sprite(game.width/2,40, 'electric');
                    this.electric.animations.add('electric',[0,1,2,3],12,true);
                    game.physics.arcade.enable(this.electric);
                    this.electric.body.immovable = true;
                    this.currentOn=true;
                    this.electric.animations.play('electric');
                }
            }
        }
        if( this.keyboard.b.isDown && this.currentOn==true ){
            if(player.tool1>0)
            {
                this.electric.kill();
                this.currentOn=false;
                player.tool1-=1;
                this.machine1.power=false;
                this.machine2.power=false;
            }
        }
    },
    batteryEffect: function(player, battery){
        if(this.keyboard.c.isDown){
            battery.kill();
            player.battery+=1;
            console.log(player.battery);
        }
    },
    shockEffect: function(){
        //elecSound.play();
        game.camera.shake(0.02, 300);
        game.camera.flash(0xffffff, 300);
        console.log('player die');
    },
    tool1Effect: function(player, tool1){
        if(this.keyboard.c.isDown){
            tool1.kill();
            player.tool1+=1;
            console.log(player.tool1);
        }
    },  
    //downcounter
    render: function () {
        if (timer.running) {
            game.debug.text(this.formatTime(Math.round((timerEvent.delay - timer.ms) / 1000)), 2, 14, "#ff0");
        }
        else {
            this.player.alive = false;
        }
    },
    endTimer: function() {
        timer.stop();
    },
    formatTime: function(s) {
        var minutes = "0" + Math.floor(s / 60);
        var seconds = "0" + (s - minutes * 60);
        return minutes.substr(-2) + ":" + seconds.substr(-2);   
    }
};